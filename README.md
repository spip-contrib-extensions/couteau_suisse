# Le Couteau Suisse

![Le Couteau Suisse](/prive/themes/spip/images/couteau-xx.svg)

Réunit en un seul plugin une liste de petites fonctionnalités nouvelles et utiles améliorant la gestion de votre site SPIP.

Chacun de ces outils peut être activé ou non par l’utilisateur sur la page d’administration du plugin et gérer un certain nombre de variables : cliquer sur **Configuration**, puis choisir l’onglet **Le Couteau Suisse**.

Les catégories disponibles sont : Administration, Sécurité, Interface privée, Améliorations des textes, Raccourcis typographiques, Affichage public, Balises, filtres, critères.

Découvrez dans ce plugin vos outils favoris : *Blocs dépliables*, *Exposants typographiques*, *Guillemets typographiques*, *Belles puces*, *Lutte contre le SPAM*, *Mailcrypt*, *Belles URLs*, *SPIP et les liens... externes*, *Smileys*, *Un sommaire pour vos articles*, *Découpe en pages et onglets*, etc., etc.


## Documentation

Sur [SPIP Contrib](https://contrib.spip.net/?article2166)
